package net.loyin.app.ctrl.admin;

import net.loyin.annotation.ControllerBind;
import net.loyin.annotation.CtrlMethod;
import net.loyin.app.model.Album;
import net.loyin.app.model.AlbumCate;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 相册
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route = "/admin/album")
public class AlbumCtrl extends AdminBaseCtrl {

    public void index(){
        String kwd=this.getPara("kwd");
        int pageNumber=this.getParaToInt("pageNumber",1);
        int pageSize=this.getParaToInt("pageSize",20);
        List<Object> paras=new ArrayList<Object>();
        StringBuffer sql=new StringBuffer("select u.*,c.name cate_name from ");
        sql.append(Album.tableName);
        sql.append(" u,");
        sql.append(AlbumCate.tableName);
        sql.append(" c where c.id=u.cate_id ");
        if(StringUtils.isNotBlank(kwd)){
            sql.append(" and (u.title like ? or u.sup_title like ?)");
            kwd="%"+kwd+"%";
            paras.add(kwd);
            paras.add(kwd);
        }
        Integer status=this.getParaToInt("status");
        if(status!=null){
            sql.append(" and u.status=? ");
            paras.add(status);
        }
        String cate_id=this.getPara("cate_id");
        if(StringUtils.isNotBlank(cate_id)){
            sql.append(" and u.cate_id=? ");
            paras.add(cate_id);
        }
        this.keepPara();
        setCateList();
        this.setAttr("page",Album.dao.paginate(pageNumber,pageSize,sql.toString(),paras.toArray()));
    }
    private void setCateList(){
        this.setAttr("cateList",AlbumCate.dao.find("select * from album_cate where status=1 order by sort asc"));
    }
    public void add(){
        String id=this.getPara(0);
        if(StringUtils.isNotBlank(id)){
            this.setAttr("po",Album.dao.findById(id));
        }
        setCateList();
    }
    @CtrlMethod(isAjax = true)
    public void save(){
        Album entity=this.getModel(Album.class);
        if(entity!=null){
            String id=entity.getId();
            if(StringUtils.isEmpty(id)) {
                entity.setCreateDatetime(new Date());
                entity.save();
            }else{
                entity.update();
            }
            id=entity.getId();
            this.renderJsonMsg(JSON_RESULT_SUCCESS,"保存成功!");
        }else{
            this.renderJsonMsg(JSON_RESULT_WARNNING,"参数错误!");
        }
    }
    @CtrlMethod(isAjax = true)
    public void del(){
        String id=this.getPara(0);
        try {
            if (StringUtils.isNotBlank(id)) {
                Album.dao.deleteById(id);
                this.renderJsonMsg(JSON_RESULT_SUCCESS, "删除成功!");
            } else {
                this.renderJsonMsg(JSON_RESULT_WARNNING, "缺少参数!");
            }
        }catch (Exception e){
            logger.error("删除异常",e);
            this.renderJsonMsg(JSON_RESULT_ERROR, "删除异常,可能存在引用,请先删除对应的数据!");
        }
    }
}
