package net.loyin.app.ctrl.admin;

import com.baidu.ueditor.ActionEnter;
import net.loyin.annotation.ControllerBind;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 文件上传下载处理
 * Created by loyin on 16/1/15.
 */
@ControllerBind(route = "/admin/file")
public class FileCtrl extends AdminBaseCtrl {
    /**百度UE 编辑器上传*/
    public void upload4UE(){
        HttpServletRequest request=this.getRequest();
        HttpServletResponse response=this.getResponse();
        response.setHeader("Content-Type" , "text/html");
        String rootPath = request.getRealPath( "/" );
        this.renderText( new ActionEnter( request, rootPath ).exec() );
    }
    /**单独上传图片*/
    public void uploadImg(){

    }
    /**单独上传文件*/
    public void uploadFile(){

    }
    /**下载文件*/
    public void download(){

    }
}
