package net.loyin.app.icp;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import net.loyin.annotation.CtrlMethod;
import net.loyin.app.ctrl.admin.AdminBaseCtrl;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * 管理端拦截器
 * Created by loyin on 16/1/12.
 */
public class AdminInterceptor implements Interceptor {
    private static Logger logger= Logger.getLogger(AdminInterceptor.class);
    @Override
    public void intercept(Invocation invocation) {
        AdminBaseCtrl ctrl=(AdminBaseCtrl)invocation.getController();
        ctrl.setAttr("admin_login_user",ctrl.getUser());
        String actionKey=invocation.getActionKey();
        CtrlMethod ctrlMethod=(CtrlMethod)invocation.getMethod().getAnnotation(CtrlMethod.class);
        Date now=new Date();
        ctrl.setAttr("nowtime",now.getTime());
        ctrl.setAttr("now",now);
        String userid=ctrl.getUserId();
        if(ctrlMethod!=null){
            //是否是ajax访问 用户登录超时提示 json
            if(ctrlMethod.isAjax()){
                if(StringUtils.isEmpty(userid)){
                    ctrl.renderJsonMsg(ctrl.JSON_RESULT_LOGIN_TIMEOUT,"登录超时,请重新登陆!");
                }
            }else{
                if(StringUtils.isEmpty(userid))
                ctrl.forwardAction("/admin/timeOut");
            }
        }
        invocation.invoke();
    }
}
