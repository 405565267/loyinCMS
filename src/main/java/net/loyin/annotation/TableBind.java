package net.loyin.annotation;

import com.jfinal.plugin.activerecord.DbKit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 表绑定
 * Created by loyin on 16/1/12.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface TableBind {
    /**表名*/
    String tableName();
    /**主键*/
    String primaryKey() default "id";
    /**对应的数据源的配置名称 datasource */
    String dataSource() default DbKit.MAIN_CONFIG_NAME;
}
