
/* Drop Tables */

DROP TABLE IF EXISTS ALBUM;
DROP TABLE IF EXISTS ALBUM_CATE;
DROP TABLE IF EXISTS ARTICLE;
DROP TABLE IF EXISTS ARTICLE_CATE;
DROP TABLE IF EXISTS NAV_MENU;
DROP TABLE IF EXISTS SYS_DEPARTMENT;
DROP TABLE IF EXISTS SYS_DICT_DETAIL;
DROP TABLE IF EXISTS SYS_DICT;
DROP TABLE IF EXISTS SYS_ROLE_MENU;
DROP TABLE IF EXISTS SYS_MENU;
DROP TABLE IF EXISTS SYS_OPLOG;
DROP TABLE IF EXISTS SYS_USER_ROLE;
DROP TABLE IF EXISTS SYS_ROLE;
DROP TABLE IF EXISTS SYS_USER;




/* Create Tables */

CREATE TABLE ALBUM
(
	ID VARCHAR(50) NOT NULL,
	CATE_ID VARCHAR(50) NOT NULL,
	TITLE VARCHAR(50) NOT NULL,
	SUP_TITLE VARCHAR(100),
	SUMMARY TEXT,
	CONTENT TEXT NOT NULL,
	IMG VARCHAR(100),
	IMG_LIST TEXT NOT NULL,
	CREATE_DATETIME TIMESTAMP NOT NULL,
	STATUS SMALLINT DEFAULT 0 NOT NULL,
	READ_COUT BIGINT DEFAULT 0 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE ALBUM_CATE
(
	ID VARCHAR(50) NOT NULL,
	PARENTID VARCHAR(50),
	NAME VARCHAR(50) NOT NULL,
	EN_NAME VARCHAR(50),
	SUMMARY TEXT,
	-- 排序号
	SORT SMALLINT DEFAULT 0 NOT NULL,
	-- 级别
	LEVELNO SMALLINT DEFAULT 1 NOT NULL,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	CODE VARCHAR(50) NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE ARTICLE
(
	ID VARCHAR(50) NOT NULL,
	CATE_ID VARCHAR(50) NOT NULL,
	TITLE VARCHAR(50) NOT NULL,
	SUP_TITLE VARCHAR(100),
	SUMMARY TEXT,
	CONTENT TEXT NOT NULL,
	IMG VARCHAR(100),
	CREATE_DATETIME TIMESTAMP NOT NULL,
	STATUS SMALLINT DEFAULT 0 NOT NULL,
	READ_COUT BIGINT DEFAULT 0 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE ARTICLE_CATE
(
	ID VARCHAR(50) NOT NULL,
	PARENTID VARCHAR(50),
	CODE VARCHAR(50) NOT NULL,
	NAME VARCHAR(50) NOT NULL,
	-- 排序号
	SORT SMALLINT DEFAULT 0 NOT NULL,
	-- 级别
	LEVELNO SMALLINT DEFAULT 1 NOT NULL,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	EN_NAME VARCHAR(50),
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE NAV_MENU
(
	ID VARCHAR(50) NOT NULL,
	PARENTID VARCHAR(50) NOT NULL,
	NAME VARCHAR(50),
	EN_NAME VARCHAR(50),
	-- 排序号
	SORT SMALLINT DEFAULT 0 NOT NULL,
	-- 级别
	LEVELNO SMALLINT DEFAULT 1 NOT NULL,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	TYPE SMALLINT DEFAULT 0 NOT NULL,
	-- 实际跳转的url
	JUMP_URL VARCHAR(100),
	URL VARCHAR(100) NOT NULL UNIQUE,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_DEPARTMENT
(
	ID VARCHAR(50) NOT NULL,
	TYPE SMALLINT NOT NULL,
	NAME VARCHAR(50) NOT NULL,
	EN_NAME VARCHAR(50),
	DIRECTOR VARCHAR(50),
	SUMMARY TEXT,
	-- 排序号
	SORT SMALLINT DEFAULT 0 NOT NULL,
	PARENTID VARCHAR(50),
	-- 级别
	LEVELNO SMALLINT DEFAULT 1 NOT NULL,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_DICT
(
	ID VARCHAR(50) NOT NULL,
	-- 0:单列
	-- 1:树形
	DICTTYPE SMALLINT DEFAULT 0 NOT NULL,
	NAME VARCHAR(50) NOT NULL,
	EN_NAME VARCHAR(50),
	CODE VARCHAR(50) NOT NULL,
	REMARK TEXT,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_DICT_DETAIL
(
	ID VARCHAR(50) NOT NULL,
	PARENTID VARCHAR(50),
	DICT_ID VARCHAR(50) NOT NULL,
	NAME VARCHAR(50) NOT NULL,
	EN_NAME VARCHAR(50),
	CODE VARCHAR(50),
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	-- 级别
	LEVELNO SMALLINT DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_MENU
(
	ID VARCHAR(50) NOT NULL,
	PARENTID VARCHAR(50),
	ICON VARCHAR(50),
	NAME VARCHAR(50) NOT NULL,
	-- 排序号
	SORT SMALLINT DEFAULT 0 NOT NULL,
	-- 级别
	LEVELNO SMALLINT DEFAULT 1 NOT NULL,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	TYPE SMALLINT DEFAULT 0 NOT NULL,
	URL VARCHAR(100),
	-- 显示的url
	URLKEY VARCHAR(50),
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_OPLOG
(
	ID VARCHAR(50) NOT NULL,
	MENU_NAME VARCHAR(50) NOT NULL,
	OPRESULT TEXT,
	REQ_DATA TEXT,
	URL VARCHAR(100) NOT NULL,
	USER_ID VARCHAR(50) NOT NULL,
	IP VARCHAR(50) NOT NULL,
	CREATE_DATETIME TIMESTAMP DEFAULT now() NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_ROLE
(
	ID VARCHAR(50) NOT NULL,
	NAME VARCHAR(50) NOT NULL,
	REMARK TEXT,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_ROLE_MENU
(
	MENU_ID VARCHAR(50) NOT NULL,
	ROLE_ID VARCHAR(50) NOT NULL
) WITHOUT OIDS;


CREATE TABLE SYS_USER
(
	ID VARCHAR(50) NOT NULL,
	ACCOUNT VARCHAR(50) NOT NULL UNIQUE,
	PASSWORD VARCHAR(50) NOT NULL,
	STATUS SMALLINT DEFAULT 1 NOT NULL,
	REALNAME VARCHAR(20),
	-- 0:未知
	-- 1:男
	-- 2:女
	SEX SMALLINT DEFAULT 0 NOT NULL,
	EMAIL VARCHAR(50),
	MOBILE VARCHAR(50),
	CREATE_DATETIME TIMESTAMP NOT NULL,
	LAST_LOGIN_IP VARCHAR(20),
	LAST_LOGIN_TIME TIMESTAMP,
	AVATOR VARCHAR(50),
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE SYS_USER_ROLE
(
	USER_ID VARCHAR(50) NOT NULL,
	ROLE_ID VARCHAR(50) NOT NULL
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE ALBUM
	ADD FOREIGN KEY (CATE_ID)
	REFERENCES ALBUM_CATE (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ALBUM_CATE
	ADD FOREIGN KEY (PARENTID)
	REFERENCES ALBUM_CATE (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ARTICLE
	ADD FOREIGN KEY (CATE_ID)
	REFERENCES ARTICLE_CATE (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ARTICLE_CATE
	ADD FOREIGN KEY (PARENTID)
	REFERENCES ARTICLE_CATE (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE NAV_MENU
	ADD FOREIGN KEY (PARENTID)
	REFERENCES NAV_MENU (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_DICT_DETAIL
	ADD FOREIGN KEY (DICT_ID)
	REFERENCES SYS_DICT (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_DICT_DETAIL
	ADD FOREIGN KEY (PARENTID)
	REFERENCES SYS_DICT_DETAIL (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_MENU
	ADD FOREIGN KEY (PARENTID)
	REFERENCES SYS_MENU (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_ROLE_MENU
	ADD FOREIGN KEY (MENU_ID)
	REFERENCES SYS_MENU (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_ROLE_MENU
	ADD FOREIGN KEY (ROLE_ID)
	REFERENCES SYS_ROLE (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_USER_ROLE
	ADD FOREIGN KEY (ROLE_ID)
	REFERENCES SYS_ROLE (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_OPLOG
	ADD FOREIGN KEY (USER_ID)
	REFERENCES SYS_USER (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SYS_USER_ROLE
	ADD FOREIGN KEY (USER_ID)
	REFERENCES SYS_USER (ID)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



/* Comments */

COMMENT ON TABLE ALBUM IS '相册';
COMMENT ON COLUMN ALBUM.ID IS 'id';
COMMENT ON COLUMN ALBUM.CATE_ID IS 'cate_id';
COMMENT ON COLUMN ALBUM.TITLE IS '标题';
COMMENT ON COLUMN ALBUM.SUP_TITLE IS '子标题';
COMMENT ON COLUMN ALBUM.SUMMARY IS '摘要';
COMMENT ON COLUMN ALBUM.CONTENT IS '内容';
COMMENT ON COLUMN ALBUM.IMG IS '图片';
COMMENT ON COLUMN ALBUM.IMG_LIST IS '图片列表';
COMMENT ON COLUMN ALBUM.CREATE_DATETIME IS 'create_datetime';
COMMENT ON COLUMN ALBUM.STATUS IS 'status';
COMMENT ON COLUMN ALBUM.READ_COUT IS '阅览量';
COMMENT ON TABLE ALBUM_CATE IS '相册类别';
COMMENT ON COLUMN ALBUM_CATE.ID IS 'id';
COMMENT ON COLUMN ALBUM_CATE.PARENTID IS 'parentid';
COMMENT ON COLUMN ALBUM_CATE.NAME IS 'name';
COMMENT ON COLUMN ALBUM_CATE.EN_NAME IS '英文名称';
COMMENT ON COLUMN ALBUM_CATE.SUMMARY IS '摘要';
COMMENT ON COLUMN ALBUM_CATE.SORT IS 'sort : 排序号';
COMMENT ON COLUMN ALBUM_CATE.LEVELNO IS 'levelno : 级别';
COMMENT ON COLUMN ALBUM_CATE.STATUS IS 'status';
COMMENT ON COLUMN ALBUM_CATE.CODE IS 'code';
COMMENT ON TABLE ARTICLE IS '文章';
COMMENT ON COLUMN ARTICLE.ID IS 'id';
COMMENT ON COLUMN ARTICLE.CATE_ID IS '分类id';
COMMENT ON COLUMN ARTICLE.TITLE IS '标题';
COMMENT ON COLUMN ARTICLE.SUP_TITLE IS '子标题';
COMMENT ON COLUMN ARTICLE.SUMMARY IS '摘要';
COMMENT ON COLUMN ARTICLE.CONTENT IS '内容';
COMMENT ON COLUMN ARTICLE.IMG IS '图片';
COMMENT ON COLUMN ARTICLE.CREATE_DATETIME IS 'create_datetime';
COMMENT ON COLUMN ARTICLE.STATUS IS 'status';
COMMENT ON COLUMN ARTICLE.READ_COUT IS '阅览量';
COMMENT ON TABLE ARTICLE_CATE IS '文章类别';
COMMENT ON COLUMN ARTICLE_CATE.ID IS 'id';
COMMENT ON COLUMN ARTICLE_CATE.PARENTID IS 'parentid';
COMMENT ON COLUMN ARTICLE_CATE.CODE IS 'code';
COMMENT ON COLUMN ARTICLE_CATE.NAME IS 'name';
COMMENT ON COLUMN ARTICLE_CATE.SORT IS 'sort : 排序号';
COMMENT ON COLUMN ARTICLE_CATE.LEVELNO IS 'levelno : 级别';
COMMENT ON COLUMN ARTICLE_CATE.STATUS IS 'status';
COMMENT ON COLUMN ARTICLE_CATE.EN_NAME IS '英文名称';
COMMENT ON TABLE NAV_MENU IS '导航菜单';
COMMENT ON COLUMN NAV_MENU.ID IS 'id';
COMMENT ON COLUMN NAV_MENU.PARENTID IS 'parentid';
COMMENT ON COLUMN NAV_MENU.NAME IS 'name';
COMMENT ON COLUMN NAV_MENU.EN_NAME IS '英文名称';
COMMENT ON COLUMN NAV_MENU.SORT IS 'sort : 排序号';
COMMENT ON COLUMN NAV_MENU.LEVELNO IS 'levelno : 级别';
COMMENT ON COLUMN NAV_MENU.STATUS IS 'status';
COMMENT ON COLUMN NAV_MENU.TYPE IS 'type';
COMMENT ON COLUMN NAV_MENU.JUMP_URL IS 'jump_url : 实际跳转的url';
COMMENT ON COLUMN NAV_MENU.URL IS 'url';
COMMENT ON TABLE SYS_DEPARTMENT IS '部门';
COMMENT ON COLUMN SYS_DEPARTMENT.ID IS 'id';
COMMENT ON COLUMN SYS_DEPARTMENT.TYPE IS 'type';
COMMENT ON COLUMN SYS_DEPARTMENT.NAME IS 'name';
COMMENT ON COLUMN SYS_DEPARTMENT.EN_NAME IS '英文名称';
COMMENT ON COLUMN SYS_DEPARTMENT.DIRECTOR IS '负责人';
COMMENT ON COLUMN SYS_DEPARTMENT.SUMMARY IS '摘要';
COMMENT ON COLUMN SYS_DEPARTMENT.SORT IS 'sort : 排序号';
COMMENT ON COLUMN SYS_DEPARTMENT.PARENTID IS 'parentid';
COMMENT ON COLUMN SYS_DEPARTMENT.LEVELNO IS 'levelno : 级别';
COMMENT ON COLUMN SYS_DEPARTMENT.STATUS IS 'status';
COMMENT ON TABLE SYS_DICT IS '数据字典';
COMMENT ON COLUMN SYS_DICT.ID IS 'id';
COMMENT ON COLUMN SYS_DICT.DICTTYPE IS 'dicttype : 0:单列
1:树形';
COMMENT ON COLUMN SYS_DICT.NAME IS 'name';
COMMENT ON COLUMN SYS_DICT.EN_NAME IS '英文名称';
COMMENT ON COLUMN SYS_DICT.CODE IS 'code';
COMMENT ON COLUMN SYS_DICT.REMARK IS 'remark';
COMMENT ON COLUMN SYS_DICT.STATUS IS 'status';
COMMENT ON TABLE SYS_DICT_DETAIL IS '数据字典详细项';
COMMENT ON COLUMN SYS_DICT_DETAIL.ID IS 'id';
COMMENT ON COLUMN SYS_DICT_DETAIL.PARENTID IS 'parentid';
COMMENT ON COLUMN SYS_DICT_DETAIL.DICT_ID IS 'dict_id';
COMMENT ON COLUMN SYS_DICT_DETAIL.NAME IS 'name';
COMMENT ON COLUMN SYS_DICT_DETAIL.EN_NAME IS '英文名称';
COMMENT ON COLUMN SYS_DICT_DETAIL.CODE IS 'code';
COMMENT ON COLUMN SYS_DICT_DETAIL.STATUS IS 'status';
COMMENT ON COLUMN SYS_DICT_DETAIL.LEVELNO IS 'levelno : 级别';
COMMENT ON TABLE SYS_MENU IS '菜单';
COMMENT ON COLUMN SYS_MENU.ID IS 'id';
COMMENT ON COLUMN SYS_MENU.PARENTID IS 'parentid';
COMMENT ON COLUMN SYS_MENU.ICON IS '图标';
COMMENT ON COLUMN SYS_MENU.NAME IS 'name';
COMMENT ON COLUMN SYS_MENU.SORT IS 'sort : 排序号';
COMMENT ON COLUMN SYS_MENU.LEVELNO IS 'levelno : 级别';
COMMENT ON COLUMN SYS_MENU.STATUS IS 'status';
COMMENT ON COLUMN SYS_MENU.TYPE IS 'type';
COMMENT ON COLUMN SYS_MENU.URL IS 'url';
COMMENT ON COLUMN SYS_MENU.URLKEY IS 'urlkey : 显示的url';
COMMENT ON TABLE SYS_OPLOG IS '操作日志';
COMMENT ON COLUMN SYS_OPLOG.ID IS 'id';
COMMENT ON COLUMN SYS_OPLOG.MENU_NAME IS 'menu_name';
COMMENT ON COLUMN SYS_OPLOG.OPRESULT IS '操作结果';
COMMENT ON COLUMN SYS_OPLOG.REQ_DATA IS '请求数据';
COMMENT ON COLUMN SYS_OPLOG.URL IS 'url';
COMMENT ON COLUMN SYS_OPLOG.USER_ID IS 'user_id';
COMMENT ON COLUMN SYS_OPLOG.IP IS 'ip';
COMMENT ON COLUMN SYS_OPLOG.CREATE_DATETIME IS 'create_datetime';
COMMENT ON TABLE SYS_ROLE IS '角色';
COMMENT ON COLUMN SYS_ROLE.ID IS 'id';
COMMENT ON COLUMN SYS_ROLE.NAME IS 'name';
COMMENT ON COLUMN SYS_ROLE.REMARK IS 'remark';
COMMENT ON COLUMN SYS_ROLE.STATUS IS 'status';
COMMENT ON TABLE SYS_ROLE_MENU IS '角色菜单权限';
COMMENT ON COLUMN SYS_ROLE_MENU.MENU_ID IS 'menu_id';
COMMENT ON COLUMN SYS_ROLE_MENU.ROLE_ID IS 'role_id';
COMMENT ON TABLE SYS_USER IS '系统用户';
COMMENT ON COLUMN SYS_USER.ID IS 'id';
COMMENT ON COLUMN SYS_USER.ACCOUNT IS 'account';
COMMENT ON COLUMN SYS_USER.PASSWORD IS 'password';
COMMENT ON COLUMN SYS_USER.STATUS IS 'status';
COMMENT ON COLUMN SYS_USER.REALNAME IS 'realname';
COMMENT ON COLUMN SYS_USER.SEX IS 'sex : 0:未知
1:男
2:女';
COMMENT ON COLUMN SYS_USER.EMAIL IS 'email';
COMMENT ON COLUMN SYS_USER.MOBILE IS 'mobile';
COMMENT ON COLUMN SYS_USER.CREATE_DATETIME IS 'create_datetime';
COMMENT ON COLUMN SYS_USER.LAST_LOGIN_IP IS 'last_login_ip';
COMMENT ON COLUMN SYS_USER.LAST_LOGIN_TIME IS 'last_login_time';
COMMENT ON COLUMN SYS_USER.AVATOR IS '头像';
COMMENT ON TABLE SYS_USER_ROLE IS 'sys_user_role';
COMMENT ON COLUMN SYS_USER_ROLE.USER_ID IS 'user_id';
COMMENT ON COLUMN SYS_USER_ROLE.ROLE_ID IS 'role_id';



